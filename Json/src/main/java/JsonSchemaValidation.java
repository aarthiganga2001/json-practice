import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class JsonSchemaValidation {

    public static void main(String[] args) throws FileNotFoundException {

        File schemaFile=new File("json-schema.json");
        JSONTokener schemaData=new JSONTokener(new FileInputStream(schemaFile));
        JSONObject jsonSchema=new JSONObject(schemaData);

        File jsonFile=new File("json-schema-data.json");
        JSONTokener jsonData=new JSONTokener(new FileInputStream(jsonFile));
        JSONObject jsonDataObject=new JSONObject(jsonData);

        Schema schemaValidator= SchemaLoader.load(jsonSchema);
        schemaValidator.validate(jsonDataObject);

        System.out.println("success");

    }
}
